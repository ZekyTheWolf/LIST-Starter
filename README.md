# NEW LARAVEL 10!
#### If you want to use Laravel 9, switch branch and download it / use gitlab commands.

# LIST Start pack

![NodeJS](https://img.shields.io/badge/NodeJS-18.x-%23fff?style=for-the-badge)
![PHP](https://img.shields.io/badge/PHP-8.2.x-%23fff?style=for-the-badge)
![Laravel](https://img.shields.io/badge/Laravel-10.0-%23fff?style=for-the-badge)

#### Laravel, Inertia, Svelte, TypeScript with TailwindCSS, Flowbite.

## Download
#### For download, you can use one of the Gitlab methods (Gitlab CLI/SSH/HTTPS/Github Desktop)

## Install via composer
#### This will automaticly install all php dependencies & create necessary things for laravel
```
composer create-project zekythewolf/list
```

## Install npm dependencies
```
npm install
```

## Running aplication
```
php artisan serve
npm run dev
```

### And you should be ready to go.

[Apache2](./.docs/apache.md)
